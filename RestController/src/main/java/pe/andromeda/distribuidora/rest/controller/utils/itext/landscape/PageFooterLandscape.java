package pe.andromeda.distribuidora.rest.controller.utils.itext.landscape;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class PageFooterLandscape extends PdfPageEventHelper
{
	private Font FontTemplate = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
	private String TitleLeft;
	private String TitleCenter;
	private String TitleRigth;
	private Integer PositionX;
	private Integer PositionY;
	private Integer TableWith;

	public void setTitleLeft(String titleLeft)
	{
		TitleLeft = titleLeft;
	}
	public void setTitleCenter(String titleCenter)
	{
		TitleCenter = titleCenter;
	}
	public void setTitleRigth(String titleRigth)
	{
		TitleRigth = titleRigth;
	}
	public void setPositionX(Integer positionX)
	{
		PositionX = positionX;
	}
	public void setPositionY(Integer positionY)
	{
		PositionY = positionY;
	}
	public void setTableWith(Integer tableWith)
	{
		TableWith = tableWith;
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document)
	{
		PdfPCell Cell;
		PdfPTable Table = new PdfPTable(3);
		try
		{
			Table.setWidths(new int[]{24, 24, 24});
			Table.setTotalWidth(TableWith == null ? 800 : TableWith);
			Table.setLockedWidth(true);
			Table.getDefaultCell().setFixedHeight(20);

			Cell = new PdfPCell();
			Cell.setBorder(Rectangle.TOP);
			Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			Cell.setPhrase(new Phrase(TitleLeft, FontTemplate));
			Table.addCell(Cell);

			Cell = new PdfPCell();
			Cell.setBorder(Rectangle.TOP);
			Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			Cell.setPhrase(new Phrase(TitleCenter, FontTemplate));
			Table.addCell(Cell);

			Cell = new PdfPCell();
			Cell.setBorder(Rectangle.TOP);
			Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			Cell.setPhrase(new Phrase(TitleRigth, FontTemplate));
			Table.addCell(Cell);

			int xPos = PositionX == null ? 24 : PositionX;
			int yPos = PositionY == null ? 30 : PositionY;
			Table.writeSelectedRows(0, -1, xPos, yPos, writer.getDirectContent());
		}
		catch(DocumentException DocExp)
		{
			throw new ExceptionConverter(DocExp);
		}
	}
}