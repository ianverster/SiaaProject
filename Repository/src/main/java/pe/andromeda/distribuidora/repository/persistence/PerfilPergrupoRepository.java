package pe.andromeda.distribuidora.repository.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.andromeda.distribuidora.domain.persistence.PerfilPergrupo;
 
public interface PerfilPergrupoRepository extends JpaRepository<PerfilPergrupo, Integer>{

}
