package pe.andromeda.distribuidora.rest.controller.utils.itext.landscape;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class PageWatermarkLandscape extends PdfPageEventHelper
{
	Font FONT = new Font(Font.FontFamily.TIMES_ROMAN, 60, Font.ITALIC, new GrayColor(0.95f));
	private String Data;
	public void setData(String data)
	{
		Data = data;
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document)
	{
		ColumnText.showTextAligned(writer.getDirectContentUnder(), Element.ALIGN_CENTER, new Phrase(Data, FONT), 397.5f, 300, writer.getPageNumber() % 2 == 1 ? 45 : -45);
	}
}