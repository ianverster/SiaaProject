package pe.andromeda.distribuidora.repository.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pe.andromeda.distribuidora.domain.persistence.Permiso;

public interface PermisoRepository extends JpaRepository<Permiso, Integer>{

	@Query("From Permiso p where p.pergrupo.codigo = ?1")
	public List<Permiso> getPermisosByCodigoGrupo(Integer codigoGrupo);
	
}
