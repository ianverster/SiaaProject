/**
 * 
 */
package pe.andromeda.distribuidora.dataaccess.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Project-PE
 *
 */
@Configuration
@ComponentScan(value = {"pe.andromeda.distribuidora.dataaccess.impl","pe.andromeda.distribuidora.dataaccess.custom.impl"})
public class DataAccessConfiguration {

}
