package pe.andromeda.distribuidora.rest.controller.utils.itext.portland;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

public class PageHeaderPortland extends PdfPageEventHelper
{
	Font FontTemplate = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
	private PdfTemplate Template;
	private String TitleLeft;
	private Image LogoLeft;
	private String TitleCenter;
	private Integer PositionX;
	private Integer PositionY;
	private Integer TableWith;

	public void setTitleLeft(String titleLeft)
	{
		TitleLeft = titleLeft;
	}
	public void setTitleCenter(String titleCenter)
	{
		TitleCenter = titleCenter;
	}
	public void setPositionX(Integer positionX)
	{
		PositionX = positionX;
	}
	public void setPositionY(Integer positionY)
	{
		PositionY = positionY;
	}
	public void setTableWith(Integer tableWith)
	{
		TableWith = tableWith;
	}
	public void onOpenDocument(PdfWriter Writer, Document Documento)
	{
		Template = Writer.getDirectContent().createTemplate(30, 16);
	}
	public void setLogoLeft(Image logoLeft) {
		LogoLeft = logoLeft;
	}
	public void onEndPage(PdfWriter Writer, Document Documento)//AGRREGA LA CABECERA A LA PAGINAA
	{
		PdfPCell Cell;
		PdfPTable Table = new PdfPTable(4);
		try
		{
			Table.setWidths(new int[] { 24, 24, 24, 2 });
			Table.setTotalWidth(TableWith == null ? 547 : TableWith);
			Table.setLockedWidth(true);
			Table.getDefaultCell().setFixedHeight(10);
			Table.getDefaultCell().setBorder(Rectangle.BOTTOM);
			
			Chunk chunk = new Chunk(LogoLeft, 0, -16);
			Table.addCell(new Phrase(chunk));
			//Table.addCell(new Phrase(TitleLeft, FontTemplate));

			Cell = new PdfPCell();
			Cell.setBorder(Rectangle.BOTTOM);
			Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			Cell.setPhrase(new Phrase(TitleCenter, FontTemplate));
			Table.addCell(Cell);
			Table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);

			Cell = new PdfPCell(Image.getInstance(Template));
			Table.addCell(new Phrase(String.format("PAGINA %d DE", Writer.getPageNumber()), FontTemplate));
			Cell.setBorder(Rectangle.BOTTOM);
			Table.addCell(Cell);
			int xPos = PositionX == null ? 24 : PositionX;
			int yPos = PositionY == null ? 830 : PositionY;
			Table.writeSelectedRows(0, -1, xPos, yPos, Writer.getDirectContent());
		}
		catch (DocumentException DocExp)
		{
			throw new ExceptionConverter(DocExp);
		}
	}

	public void onCloseDocument(PdfWriter Writer, Document Documento) //SET TOTAL PAGES
	{
		int PositionX = 6, PositionY = 2, Rotation = 0;
		ColumnText.showTextAligned(Template, Element.ALIGN_LEFT, new Phrase(String.valueOf(Writer.getPageNumber() - 1), FontTemplate), PositionY, PositionX, Rotation);
	}
}