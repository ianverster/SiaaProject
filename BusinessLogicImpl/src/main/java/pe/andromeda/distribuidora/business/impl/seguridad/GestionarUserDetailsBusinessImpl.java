/**
 * 
 */
package pe.andromeda.distribuidora.business.impl.seguridad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pe.andromeda.distribuidora.business.seguridad.GestionarUserDetailsBusiness;
import pe.andromeda.distribuidora.dataaccess.seguridad.UsuarioDao;

/**
 * @author Johnny
 *
 */
@Service
public class GestionarUserDetailsBusinessImpl implements GestionarUserDetailsBusiness {
 
	@Autowired
	private UsuarioDao usuarioDao;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {

		UserDetails userDetails = usuarioDao.getUsuarioAdminByName(username);

		if (userDetails == null) {
			throw new UsernameNotFoundException("exception.UserDetails.usuario.existe");
		}

		return userDetails;
	}

}
