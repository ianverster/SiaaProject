/**
 * 
 */
package pe.andromeda.distribuidora.dataaccess.seguridad;

import pe.andromeda.distribuidora.business.seguridad.dto.PerfilDto;
import pe.andromeda.distribuidora.business.seguridad.dto.PermisoUsuarioDto;

/**
 * @author Johnny
 *
 */
public interface PermisoDao {
	
	public PermisoUsuarioDto getPermisos(PerfilDto perfilUsuario);

}
