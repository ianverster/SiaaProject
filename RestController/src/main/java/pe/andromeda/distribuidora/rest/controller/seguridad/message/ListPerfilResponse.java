package pe.andromeda.distribuidora.rest.controller.seguridad.message;

import java.util.List;

import pe.andromeda.distribuidora.business.seguridad.dto.PerfilDto;
import pe.andromeda.distribuidora.rest.controller.message.ResponseBody;

public class ListPerfilResponse extends ResponseBody{
	
	private List<PerfilDto> perfil;

	public List<PerfilDto> getPerfil() {
		return perfil;
	}

	public void setPerfil(List<PerfilDto> perfil) {
		this.perfil = perfil;
	}
	
}
