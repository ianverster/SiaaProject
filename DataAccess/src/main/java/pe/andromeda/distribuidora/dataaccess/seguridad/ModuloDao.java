/**
 * 
 */
package pe.andromeda.distribuidora.dataaccess.seguridad;

import pe.andromeda.distribuidora.business.seguridad.dto.ModuloUsuarioDto;
import pe.andromeda.distribuidora.business.seguridad.dto.UsuarioAdminDto;

/**
 * @author Johnny
 *
 */
public interface ModuloDao {

	public ModuloUsuarioDto getModulos(UsuarioAdminDto usuario);
}
