package pe.andromeda.distribuidora.rest.controller.seguridad.message;

import pe.andromeda.distribuidora.business.seguridad.dto.PerfilDto;
import pe.andromeda.distribuidora.rest.controller.message.RequestBody;

public class PerfilRequest extends RequestBody{
	
	private PerfilDto perfil;

	public PerfilDto getPerfil() {
		return perfil;
	}

	public void setPerfil(PerfilDto perfil) {
		this.perfil = perfil;
	}
	
}
