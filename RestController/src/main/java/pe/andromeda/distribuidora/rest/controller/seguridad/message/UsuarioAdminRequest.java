package pe.andromeda.distribuidora.rest.controller.seguridad.message;

import pe.andromeda.distribuidora.business.seguridad.dto.UsuarioAdminDto;
import pe.andromeda.distribuidora.rest.controller.message.RequestBody;

public class UsuarioAdminRequest extends RequestBody {

	private UsuarioAdminDto usuario;

	public UsuarioAdminDto getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioAdminDto usuario) {
		this.usuario = usuario;
	}
	
}
