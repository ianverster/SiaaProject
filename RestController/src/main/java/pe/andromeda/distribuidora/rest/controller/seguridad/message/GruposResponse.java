package pe.andromeda.distribuidora.rest.controller.seguridad.message;

import java.util.List;

import pe.andromeda.distribuidora.business.seguridad.dto.GruposDto;
import pe.andromeda.distribuidora.rest.controller.message.ResponseBody;

public class GruposResponse extends ResponseBody{
	
	private List<GruposDto> grupos;

	public List<GruposDto> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<GruposDto> grupos) {
		this.grupos = grupos;
	}	

}
