package pe.andromeda.distribuidora.domain.persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the sistema_permisos database table.
 * 
 */
@Entity
@Table(name="permisos")
public class Permiso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int codigo;

	private String descripcion;
	private String nombre;

//	@OneToMany(mappedBy="permiso")
//	private List<PerfilPermiso> perfilPermisos;
	
	@ManyToOne
	@JoinColumn(name="grupo")
	private Pergrupo pergrupo;

	public Permiso() {
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

//	public List<PerfilPermiso> getPerfilPermisos() {
//		return perfilPermisos;
//	}
//
//	public void setPerfilPermisos(List<PerfilPermiso> perfilPermisos) {
//		this.perfilPermisos = perfilPermisos;
//	}

	public Pergrupo getPergrupo() {
		return pergrupo;
	}

	public void setPergrupo(Pergrupo pergrupo) {
		this.pergrupo = pergrupo;
	}

	
}