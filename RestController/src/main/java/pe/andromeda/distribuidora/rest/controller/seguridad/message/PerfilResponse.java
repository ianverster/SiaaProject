package pe.andromeda.distribuidora.rest.controller.seguridad.message;

import pe.andromeda.distribuidora.business.seguridad.dto.PerfilDto;
import pe.andromeda.distribuidora.rest.controller.message.ResponseBody;

public class PerfilResponse extends ResponseBody{
	
	private String message;
	private PerfilDto perfil;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public PerfilDto getPerfil() {
		return perfil;
	}
	public void setPerfil(PerfilDto perfil) {
		this.perfil = perfil;
	}
	
}
