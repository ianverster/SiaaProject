/**
 * 
 */
package pe.andromeda.distribuidora.dataaccess.impl;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import pe.andromeda.distribuidora.common.utility.Logger;
import pe.andromeda.distribuidora.dataaccess.HistoryDao;

/**
 * @author Johnny
 *
 */
@Component
@Transactional
public class HistoryDaoImpl implements HistoryDao {

	@Override
	public void saveHistory(String methodName, Date accessDate, String userName) {
		Logger.info(this, "Save history method");

		Logger.info(this, methodName);
		Logger.info(this, String.valueOf(accessDate));
		Logger.info(this, userName);
	}

}
