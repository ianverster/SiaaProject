package pe.andromeda.distribuidora.domain.persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the perfil_pergrupo database table.
 * 
 */
@Entity
@Table(name="perfil_pergrupo")
public class PerfilPergrupo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int codigo;

	//bi-directional many-to-one association to Perfil
	@ManyToOne
	@JoinColumn(name="perfil")
	private Perfil perfil;

	//bi-directional many-to-one association to Pergrupo
	@ManyToOne
	@JoinColumn(name="grupo")
	private Pergrupo pergrupo;

	public PerfilPergrupo() {
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Pergrupo getPergrupo() {
		return this.pergrupo;
	}

	public void setPergrupo(Pergrupo pergrupo) {
		this.pergrupo = pergrupo;
	}

}