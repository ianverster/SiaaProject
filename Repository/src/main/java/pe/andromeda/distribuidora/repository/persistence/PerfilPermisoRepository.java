package pe.andromeda.distribuidora.repository.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pe.andromeda.distribuidora.domain.persistence.PerfilPermiso;

public interface PerfilPermisoRepository extends JpaRepository<PerfilPermiso, Integer>{
 
	@Query("SELECT pp.permiso.nombre FROM PerfilPermiso pp WHERE pp.perfil.codigo = ?1")
	public List<String> getListPermisosPerfil(Integer codigoPerfil);
}
