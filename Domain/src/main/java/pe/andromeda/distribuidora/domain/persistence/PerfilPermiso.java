package pe.andromeda.distribuidora.domain.persistence;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the perfil_permiso database table.
 * 
 */
@Entity
@Table(name="perfil_permiso")
public class PerfilPermiso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int codigo;

	//bi-directional many-to-one association to Perfil
	@ManyToOne
	@JoinColumn(name="perfil")
	private Perfil perfil;

	//bi-directional many-to-one association to Permiso
	@ManyToOne
	@JoinColumn(name="permiso")
	private Permiso permiso;

	public PerfilPermiso() {
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Permiso getPermiso() {
		return permiso;
	}

	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}

}