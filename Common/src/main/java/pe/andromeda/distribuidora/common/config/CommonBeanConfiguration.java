/**
 * 
 */
package pe.andromeda.distribuidora.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Project-PE
 *
 */
@Configuration
@ComponentScan(value = { "pe.andromeda.distribuidora.common.bean" })
public class CommonBeanConfiguration {

}
