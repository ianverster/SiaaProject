package pe.andromeda.distribuidora.repository.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.andromeda.distribuidora.domain.persistence.Pergrupo;

public interface PergrupoRepository extends JpaRepository<Pergrupo, Integer>{
	
}
