/**
 * 
 */
package pe.andromeda.distribuidora.business.seguridad;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author
 *
 */
public interface GestionarUserDetailsBusiness extends UserDetailsService {

}
