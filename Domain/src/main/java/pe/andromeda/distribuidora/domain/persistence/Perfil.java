package pe.andromeda.distribuidora.domain.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * The persistent class for the usuarios_perfil database table.
 * 
 */
@Entity
@Table(name="perfil")
public class Perfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int codigo;
	
	private String descripcion;
	private String nombre;
	private Integer habilitado;
	private Integer anulado;
	private Integer isadmin;

	@OneToMany(cascade={CascadeType.MERGE,CascadeType.PERSIST,CascadeType.DETACH} , orphanRemoval=true,mappedBy="perfil")
	@LazyCollection(LazyCollectionOption.FALSE)
	@JsonManagedReference
	private List<PerfilPermiso> perfilPermisos;
	
	@OneToMany(cascade={CascadeType.MERGE,CascadeType.PERSIST,CascadeType.DETACH} , orphanRemoval=true,mappedBy="perfil")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PerfilPergrupo> perfilPergrupos;

	public Perfil() {
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<PerfilPergrupo> getPerfilPergrupos() {
		return this.perfilPergrupos;
	}

	public void setPerfilPergrupos(List<PerfilPergrupo> perfilPergrupos) {
		this.perfilPergrupos = perfilPergrupos;
	}

	public List<PerfilPermiso> getPerfilPermisos() {
		return this.perfilPermisos;
	}

	public void setPerfilPermisos(List<PerfilPermiso> perfilPermisos) {
		this.perfilPermisos = perfilPermisos;
	}

	public Integer getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Integer habilitado) {
		this.habilitado = habilitado;
	}

	public Integer getAnulado() {
		return anulado;
	}

	public void setAnulado(Integer anulado) {
		this.anulado = anulado;
	}

	public Integer getIsadmin() {
		return isadmin;
	}

	public void setIsadmin(Integer isadmin) {
		this.isadmin = isadmin;
	}
	
}