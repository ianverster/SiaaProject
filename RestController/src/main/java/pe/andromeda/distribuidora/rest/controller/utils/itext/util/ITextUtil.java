package pe.andromeda.distribuidora.rest.controller.utils.itext.util;
import org.springframework.stereotype.Component;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;

@Component
public class ITextUtil
{
	private Font FontData = new Font(Font.FontFamily.HELVETICA,6,Font.NORMAL);
	private Font FontData8 = new Font(Font.FontFamily.HELVETICA,8,Font.NORMAL);
	private PdfPCell Cell;
	
	public PdfPCell TitleCell(String Data, Integer... Alignment)
	{
		Cell = new PdfPCell();
		Cell.setHorizontalAlignment(Alignment.length > 0 ? Alignment[0] : Element.ALIGN_LEFT);
		Cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
		Cell.setPhrase(new Phrase(Data, FontData));
		Cell.setPadding(4);
		return Cell;
	}
	
	public PdfPCell TitleCell8(String Data, Integer... Alignment)
	{
		Cell = new PdfPCell();
		Cell.setHorizontalAlignment(Alignment.length > 0 ? Alignment[0] : Element.ALIGN_LEFT);
		Cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
		Cell.setPhrase(new Phrase(Data, FontData8));
		Cell.setPadding(4);
		return Cell;
	}
	
	public PdfPCell ContentCell(String Data, Integer... Alignment)
	{
		Cell = new PdfPCell();
		Cell.setHorizontalAlignment(Alignment.length > 0 ? Alignment[0] : Element.ALIGN_LEFT);
		Cell.setPhrase(new Phrase(Data, FontData));
		Cell.setPadding(4);
		return Cell;
	}
	
	public PdfPCell ContentCell8(String Data, Integer... Alignment)
	{
		Cell = new PdfPCell();
		Cell.setHorizontalAlignment(Alignment.length > 0 ? Alignment[0] : Element.ALIGN_LEFT);
		Cell.setPhrase(new Phrase(Data, FontData8));
		Cell.setPadding(4);
		return Cell;
	}
	
	public PdfPCell ContentColspanCell(String Data, Integer Colspan, Integer... Alignment)
	{
		Cell = new PdfPCell();
		Cell.setHorizontalAlignment(Alignment.length > 0 ? Alignment[0] : Element.ALIGN_LEFT);
		Cell.setPhrase(new Phrase(Data, FontData));
		Cell.setColspan(Colspan);
		Cell.setPadding(4);
		return Cell;
	}
}