package pe.andromeda.distribuidora.rest.controller.seguridad.message;

import java.util.List;

import pe.andromeda.distribuidora.business.seguridad.dto.UsuarioDto;
import pe.andromeda.distribuidora.rest.controller.message.ResponseBody;

public class ListUsuarioResponse extends ResponseBody{

	private List<UsuarioDto> usuario;

	public List<UsuarioDto> getUsuario() {
		return usuario;
	}

	public void setUsuario(List<UsuarioDto> usuario) {
		this.usuario = usuario;
	}
	
}
