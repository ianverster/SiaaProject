package pe.andromeda.distribuidora.rest.controller.utils.itext.landscape;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

public class PageHeaderLandscape extends PdfPageEventHelper
{
	Font FontTemplate = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
	private PdfTemplate Template;
	private Integer PositionX;
	private Image LogoLeft;
	private Integer PositionY;
	private Integer TableWith;
	private String TitleLeft;
	private String TitleCenter;

	public void setTitleLeft(String titleLeft)
	{
		TitleLeft = titleLeft;
	}
	public void setPositionX(Integer positionX)
	{
		PositionX = positionX;
	}
	public void setPositionY(Integer positionY)
	{
		PositionY = positionY;
	}
	public void setTitleCenter(String titleCenter)
	{
		TitleCenter = titleCenter;
	}
	public void setTableWith(Integer tableWith)
	{
		TableWith = tableWith;
	}
	public void setLogoLeft(Image logoLeft) {
		LogoLeft = logoLeft;
	}
	public void onOpenDocument(PdfWriter writer, Document document) //SET COUNT PAGES
	{
		Template = writer.getDirectContent().createTemplate(30, 16);
	}

	public void onEndPage(PdfWriter Writer, Document document) //SET HEADER
	{
		PdfPCell Cell;
		PdfPTable Table = new PdfPTable(4);
		try
		{
			Table.setWidths(new int[] { 24, 24, 24, 2 });
			Table.setTotalWidth(TableWith == null ? 800 : TableWith);
			//Table.setTotalWidth(document.getPageSize().getRight() - document.getPageSize().getLeft());
			Table.setLockedWidth(true);
			Table.getDefaultCell().setFixedHeight(10);
			Table.getDefaultCell().setBorder(Rectangle.BOTTOM);
			
			Chunk chunk = new Chunk(LogoLeft, 0, -16);
			Table.addCell(new Phrase(chunk));
			
			//Table.addCell(new Phrase(TitleLeft, FontTemplate));

			Cell = new PdfPCell();
			Cell.setBorder(Rectangle.BOTTOM);
			Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			Cell.setPhrase(new Phrase(TitleCenter, FontTemplate));
			Table.addCell(Cell);
			Table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);

			Cell = new PdfPCell(Image.getInstance(Template));
			Table.addCell(new Phrase(String.format("PAGINA %d DE", Writer.getPageNumber()), FontTemplate));
			Cell.setBorder(Rectangle.BOTTOM);
			Table.addCell(Cell);

			int xPos = PositionX == null ? 20 : PositionX;
			int yPos = PositionY == null ? 580 : PositionY;
			Table.writeSelectedRows(0, -1, xPos, yPos, Writer.getDirectContent());
		}
		catch(DocumentException DocExc)
		{
			throw new ExceptionConverter(DocExc);
		}
	}

	public void onCloseDocument(PdfWriter writer, Document document)//SET TOTAL PAGES
	{
		int PositionX = 2, PositionY = 6, Rotation = 0;
		ColumnText.showTextAligned(Template, Element.ALIGN_LEFT, new Phrase(String.valueOf(writer.getPageNumber() - 1), FontTemplate), PositionX, PositionY, Rotation);
		//ColumnText.showTextAligned(Template, Element.ALIGN_LEFT, new Phrase(String.valueOf(writer.getPageNumber() - 1), FontTemplate), (document.right() - document.left()) / 2 + document.leftMargin(), document.top() + 10, 0);
	}
}