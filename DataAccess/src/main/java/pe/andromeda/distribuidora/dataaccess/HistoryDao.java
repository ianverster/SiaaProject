/**
 * 
 */
package pe.andromeda.distribuidora.dataaccess;

import java.util.Date;

/**
 * @author Johnny
 *
 */
public interface HistoryDao {

	public void saveHistory(String methodName, Date accessDate, String userName);
}
