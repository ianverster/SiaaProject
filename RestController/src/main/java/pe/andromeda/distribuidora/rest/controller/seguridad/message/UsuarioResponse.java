package pe.andromeda.distribuidora.rest.controller.seguridad.message;

import pe.andromeda.distribuidora.business.seguridad.dto.UsuarioDto;
import pe.andromeda.distribuidora.rest.controller.message.ResponseBody;

public class UsuarioResponse extends ResponseBody {

	private String message;
	private UsuarioDto usuario;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public UsuarioDto getUsuario() {
		return usuario;
	}
	public void setUsuario(UsuarioDto usuario) {
		this.usuario = usuario;
	} 
	
}
