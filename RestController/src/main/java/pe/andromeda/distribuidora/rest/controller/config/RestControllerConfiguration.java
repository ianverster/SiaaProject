/**
 * 
 */
package pe.andromeda.distribuidora.rest.controller.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class to define package to scan with Spring framework
 * 
 * @author Project-PE
 *
 */
@Configuration
@ComponentScan(basePackages = "pe.andromeda.distribuidora.rest.controller")
public class RestControllerConfiguration {

}
