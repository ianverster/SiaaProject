package pe.andromeda.distribuidora.domain.persistence;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the pergrupos database table.
 * 
 */
@Entity
@Table(name="pergrupos")
public class Pergrupo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int codigo;

	private String descripcion;

	private String nombre;

//	@OneToMany(mappedBy="pergrupo")
//	private List<PerfilPergrupo> perfilPergrupos;

//	@OneToMany(mappedBy="pergrupo")
//	private List<Permiso> permisos;

	public Pergrupo() {
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

//	public List<PerfilPergrupo> getPerfilPergrupos() {
//		return this.perfilPergrupos;
//	}
//
//	public void setPerfilPergrupos(List<PerfilPergrupo> perfilPergrupos) {
//		this.perfilPergrupos = perfilPergrupos;
//	}

//	public PerfilPergrupo addPerfilPergrupo(PerfilPergrupo perfilPergrupo) {
//		getPerfilPergrupos().add(perfilPergrupo);
//		perfilPergrupo.setPergrupo(this);
//
//		return perfilPergrupo;
//	}
//
//	public PerfilPergrupo removePerfilPergrupo(PerfilPergrupo perfilPergrupo) {
//		getPerfilPergrupos().remove(perfilPergrupo);
//		perfilPergrupo.setPergrupo(null);
//
//		return perfilPergrupo;
//	}

//	public List<Permiso> getPermisos() {
//		return this.permisos;
//	}
//
//	public void setPermisos(List<Permiso> permisos) {
//		this.permisos = permisos;
//	}

//	public Permiso addPermiso(Permiso permiso) {
//		getPermisos().add(permiso);
//		permiso.setPergrupo(this);
//
//		return permiso;
//	}
//
//	public Permiso removePermiso(Permiso permiso) {
//		getPermisos().remove(permiso);
//		permiso.setPergrupo(null);
//
//		return permiso;
//	}

}