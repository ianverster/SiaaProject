package pe.andromeda.distribuidora.rest.controller.seguridad.message;

import pe.andromeda.distribuidora.business.seguridad.dto.UsuarioDto;
import pe.andromeda.distribuidora.rest.controller.message.RequestBody;

public class UsuarioRequest extends RequestBody{
	
	private UsuarioDto usuario;

	public UsuarioDto getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDto usuario) {
		this.usuario = usuario;
	}	

}
