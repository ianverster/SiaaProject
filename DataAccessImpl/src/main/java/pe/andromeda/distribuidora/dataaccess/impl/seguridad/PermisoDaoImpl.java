/**
 * 
 */
package pe.andromeda.distribuidora.dataaccess.impl.seguridad;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pe.andromeda.distribuidora.business.seguridad.dto.PerfilDto;
import pe.andromeda.distribuidora.business.seguridad.dto.PermisoUsuarioDto;
import pe.andromeda.distribuidora.common.utility.Logger;
import pe.andromeda.distribuidora.dataaccess.seguridad.PermisoDao;
import pe.andromeda.distribuidora.repository.persistence.PerfilPermisoRepository;

/**
 * @author Johnny
 *
 */
// @Cacheable
@Component 
public class PermisoDaoImpl implements PermisoDao {

	@Autowired
	private PerfilPermisoRepository perfilPermisoRepository;
	
	@Override
//	@Cacheable(value = "permisosUsuario", key = "#perfilUsuario.nombre")
	public PermisoUsuarioDto getPermisos(PerfilDto perfilUsuario) {

		PermisoUsuarioDto permisoUsuario = new PermisoUsuarioDto(null,new Date());

		Logger.info(this, "Get Permisos de usuario perfil "+ perfilUsuario.getNombre());

		List<String> permisos = perfilPermisoRepository.getListPermisosPerfil(perfilUsuario.getCodigo());
		
		if(permisos != null && !permisos.isEmpty())
		for(String permiso : permisos){
			
			permisoUsuario.addPermiso(permiso);
		}
		
		
//		for (ModuloSistema modulo : moduloUsuario.getModulos()) {
//
//			if (modulo == ModuloSistema.ADMINISTRACION) {
//				permisoUsuario.addPermiso(ModuloSistema.ADMINISTRACION,"generarNuevoPedido");
//				// permisoUsuario.addPermiso(modulo, "C_USUARIO");
//			} else if (modulo == ModuloSistema.PEDIDOS) {
//				permisoUsuario.addPermiso(ModuloSistema.PEDIDOS,"generarNuevoPedido");
//				// permisoUsuario.addPermiso(modulo, "C_PEDIDO");
//			}
//		}

		return permisoUsuario;
	}

}
