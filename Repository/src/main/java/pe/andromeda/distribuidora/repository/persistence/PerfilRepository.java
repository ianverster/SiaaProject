package pe.andromeda.distribuidora.repository.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import pe.andromeda.distribuidora.domain.persistence.Perfil;

public interface PerfilRepository extends JpaRepository<Perfil, Integer>{
 
	@Query("FROM Perfil p WHERE p.nombre = ?1")
	public List<Perfil> findPerfilByName(String name);
	
	@Modifying
	@Query("UPDATE Perfil p Set p.anulado = 1 WHERE p.codigo = ?1 ")
	public Integer anularPerfil(Integer codigoPerfil);
	
	@Modifying
	@Query("UPDATE Perfil p Set p.habilitado = ?2 WHERE p.codigo = ?1 ")
	public Integer habilitarPerfil(Integer codigoPerfil,Integer habilitado);
	
	@Query("FROM Perfil p WHERE p.anulado != 1 AND p.isadmin != 1")
	public List<Perfil> findAll();
}
