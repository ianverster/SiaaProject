package pe.andromeda.distribuidora.rest.controller.utils.itext.portland;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class PageWatermarkPortland extends PdfPageEventHelper
{
	private String Data;
	public void setData(String data)
	{
		Data = data;
	}
	Font FONT = new Font(Font.FontFamily.TIMES_ROMAN, 60, Font.ITALIC, new GrayColor(0.93f));

	@Override
	public void onEndPage(PdfWriter Writer, Document Documento)
	{
		ColumnText.showTextAligned(Writer.getDirectContentUnder(), Element.ALIGN_CENTER, new Phrase(Data, FONT), 297.5f, 421, Writer.getPageNumber() % 2 == 1 ? 45 : -45);
	}
}