/**
 * 
 */
package pe.andromeda.distribuidora.repository.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * @author Project-PE
 *
 */
@Configuration
@PropertySource({ "classpath:persistence.properties" })
@EnableJpaRepositories("pe.andromeda.distribuidora.repository")
@EnableTransactionManagement
@ComponentScan(value = { "pe.andromeda.distribuidora.repository.persistence.impl" })
public class RepositoryConfiguration {

	@Autowired
	private Environment env;

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan(new String[] { "pe.andromeda.distribuidora.domain.persistence" });

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setShowSql(true);
		vendorAdapter.setGenerateDdl(false);// true
//		vendorAdapter.setDatabase(Database.MYSQL);
		vendorAdapter.setDatabase(Database.POSTGRESQL);
//		vendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQLInnoDBDialect");
		vendorAdapter.setDatabasePlatform("org.hibernate.dialect.PostgresPlusDialect");

		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		// em
		// .setPersistenceProvider(new HibernatePersistenceProvider());

		return em;
	}

	@Bean(destroyMethod = "close")
	public DataSource dataSource() {
		// env.getProperty(CONNECT_URL)
		HikariConfig config = new HikariConfig();
		
//----------------------------------------------------------------------------------------------------
//		config.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
//		config.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");//no existe properties
//		config.setDataSourceClassName("com.impossibl.postgres.jdbc.PGDataSource");
		// config.setDriverClassName("com.mysql.jdbc.Driver");

		// config.setJdbcUrl("jdbc:mysql://localhost:3306/andro");
		// config.setUsername("androuser");
		// config.setPassword("androuserp");

		config.setDriverClassName("org.postgresql.Driver");
		config.setJdbcUrl("jdbc:postgresql://localhost:5432/dbinfounsa");
		
		config.setConnectionTestQuery("SELECT 1");

		// config.setPoolName("springHikariCP");
		config.setMaximumPoolSize(50);
		config.setMaxLifetime(30000);
		config.setIdleTimeout(30000);
		// config.setMinimumIdle(20);
		
//		config.setPoolName("springHikariCP");//ian

		config.addDataSourceProperty("url", env.getProperty("jdbc.url"));
		config.addDataSourceProperty("user", env.getProperty("jdbc.user"));
		config.addDataSourceProperty("password",
				env.getProperty("jdbc.password"));

		// <prop key="useUnicode">true</prop>
		// <prop key="characterEncoding">utf-8</prop>

		config.addDataSourceProperty("cachePrepStmts",
				env.getProperty("jdbc.pool.cachePrepStmts"));
		config.addDataSourceProperty("prepStmtCacheSize",
				env.getProperty("jdbc.pool.prepStmtCacheSize"));
		config.addDataSourceProperty("prepStmtCacheSqlLimit",
				env.getProperty("jdbc.pool.prepStmtCacheSqlLimit"));
		config.addDataSourceProperty("useServerPrepStmts",
				env.getProperty("jdbc.pool.useServerPrepStmts"));
//----------------------------------------------------------------------------------------------------
		
//		config.setJdbcUrl("jdbc\\:postgresql\\://localhost\\:5432/dbinfounsa");
	    config.setUsername("postgres");
	    config.setPassword("admin");
//	    config.addDataSourceProperty("sslmode", "require");
//	    config.setMaximumPoolSize(3);
//	    config.setMinimumIdle(1);
	    
//-----------------------------------------------------------------------------------------
		
		HikariDataSource dataSource = new HikariDataSource(config);

		// DriverManagerDataSource dataSource = new DriverManagerDataSource();
		// //com.zaxxer.hikari.HikariDataSource
		// dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		// dataSource.setUrl("jdbc:mysql://localhost:3306/andro");
		// dataSource.setUsername("androuser");
		// dataSource.setPassword("androuserp");

		return dataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager(
			EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);

		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	Properties additionalProperties() {
		Properties properties = new Properties();

		// properties.setProperty("hibernate.connection.provider_class",
		// "org.hibernate.hikaricp.internal.HikariCPConnectionProvider");

		// properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
		// properties.setProperty("hibernate.dialect",
		// "org.hibernate.dialect.MySQL5Dialect");
		// properties.setProperty("hibernate.connection.driver_class",
		// "org.hibernate.hikaricp.internal.HikariCPConnectionProvider");
		// properties.setProperty("connection.url",
		// "${dataSource.url}");
		// properties.setProperty("connection.driver_class",
		// "${dataSource.driverClassName}");
		// properties.setProperty("connection.username",
		// "${dataSource.username}");
		// properties.setProperty("connection.password",
		// "${dataSource.password}");
		// properties.setProperty("hibernate.jdbc.batch_size",
		// "100");
		// properties.setProperty("hibernate.order_inserts",
		// "true");
		// properties.setProperty("hibernate.order_updates",
		// "true");

		// jpaProperties.put("hibernate.hbm2ddl.auto", "create-drop");
		// jpaProperties.put("hibernate.show_sql", "true");
		// properties.setProperty("hibernate.generate_statistics", "true");
		
		properties.setProperty("hibernate.show_sql", "false");
		
		return properties;
	}
}
