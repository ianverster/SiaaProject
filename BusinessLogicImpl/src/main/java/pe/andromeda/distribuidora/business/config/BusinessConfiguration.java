/**
 * 
 */
package pe.andromeda.distribuidora.business.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author Project-PE
 *
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(value = { "pe.andromeda.distribuidora.business.impl",
		"pe.andromeda.distribuidora.business.aspect" })
public class BusinessConfiguration {

}
